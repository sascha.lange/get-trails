# Changelog

## [v0.5.0] - 2019-08-02
### Added
- Get aws config also from main account

### Changed
- Refactored code for readability

## [v0.4.0] - 2019-07-31
### Added
- Write number of regions to stdout
- Write number of sub accounts to stdout
- Write progress bar to stdout

### Changed
- getTrails supports pagination to get all results from the aws organizations api