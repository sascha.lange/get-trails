package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"

	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/schollz/progressbar"
	ccsv "github.com/tsak/concurrent-csv-writer"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudtrail"
	"github.com/aws/aws-sdk-go/service/organizations"
)

var csvHeader = []string{
	"Account",
	"Region",
	"Trail Name",
	"S3 Bucket",
	"Multi Region",
	"Organization Trail",
	"Log File Validation Enabled",
}

// Default aws region for api calls that need a region
const constRegion = "eu-west-1"

const (
	constTrailName              = 0
	constTrailBucketName        = 1
	constTrailIsMultiRegion     = 2
	constTrailIsOrgTrail        = 3
	constTrailValidationEnabled = 4
)

func main() {
	// Check command line flags
	assumeRoleArg := flag.String("role", "OrganizationAccountAccessRole", "AWS assumeRole name")
	regionArg := flag.String("region", "", "Filter on AWS region name")
	accountArg := flag.String("account", "", "Filter on AWS account id")
	blacklistArg := flag.String("blacklist", "", "Provide blacklist name")
	outputfileArg := flag.String("outpout", "result.csv", "Changes the name of the created csv file")
	profileArg := flag.String("profile", "", "AWS profile name for authentication")

	flag.Parse()

	// Create and validate session
	sess := createSession(*profileArg)
	_, err := sess.Config.Credentials.Get()
	checkError("Error reading credentials\n", err)

	myBlacklist := readBlacklist(*blacklistArg)
	checkError("Error reading blacklist\n", err)

	myAccounts := createAccountList(sess, *accountArg)
	fmt.Printf("Number of discovered sub accounts: %d\n", len(myAccounts)-1)

	myRegions := createRegionList(sess, *regionArg)
	fmt.Printf("Number of discovered regions: %d\n", len(myRegions))

	fmt.Println("Start processing ...")
	bar := progressbar.New(len(myRegions) * len(myAccounts))

	currentAccountID := getCurrentAccountID(sess)

	var wg sync.WaitGroup
	wg.Add(len(myAccounts))

	csv, err := ccsv.NewCsvWriter(*outputfileArg)
	checkError("Cannot create output file\n", err)

	// Prepare error.log
	errLog, err := os.Create("error.log")
	checkError("Cannot create error.log", err)
	defer errLog.Close()
	var errMu sync.Mutex

	// Global error state
	isErr := false

	defer csv.Close()
	csv.Write(csvHeader)

	for _, account := range myAccounts {
		bar.Add(1)
		if accountBlacklisted(account, myBlacklist) {
			wg.Done()
			continue
		}
		go func(account string, csv *ccsv.CsvWriter) {
			defer wg.Done()
			for _, region := range myRegions {
				bar.Add(1)
				newSess := assumeRoleOnSubAccount(sess, currentAccountID, account, *assumeRoleArg, region)
				_, err := newSess.Config.Credentials.Get()
				if err != nil {
					isErr = true
					accessErr := fmt.Sprintf("Cannot login to account %s in region %s\n", account, region)
					message := fmt.Sprintf("%s%v\n\n", accessErr, err.Error())
					writeErrorLog(message, errLog, &errMu)
					continue
				}

				sTrails := getTrails(newSess)
				for _, trail := range sTrails {
					csv.Write([]string{
						account,
						region,
						trail[constTrailName],
						trail[constTrailBucketName],
						trail[constTrailIsMultiRegion],
						trail[constTrailIsOrgTrail],
						trail[constTrailValidationEnabled],
					})
				}
			}
		}(account, csv)
	}
	wg.Wait()
	errorInfo(isErr)
}

func errorInfo(isErr bool) {
	if isErr {
		fmt.Printf("\nErrors during run! Check your error.log\n")
	}
}

func writeErrorLog(m string, f *os.File, w *sync.Mutex) {
	w.Lock()
	defer w.Unlock()

	f.WriteString(m)
	f.Sync()
}

func assumeRoleOnSubAccount(sess *session.Session, currentAcc string, acc string, role string, reg string) *session.Session {
	var newSess *session.Session
	if currentAcc != acc {
		roleToAssumeArn := fmt.Sprintf("arn:aws:iam::%v:role/%v", acc, role)
		creds := stscreds.NewCredentials(sess, roleToAssumeArn)
		newSess = sess.Copy(&aws.Config{
			Region:      aws.String(reg),
			Credentials: creds,
		})
	} else {
		newSess = sess.Copy(&aws.Config{
			Region: aws.String(reg),
		})
	}
	return newSess
}

func readBlacklist(path string) []string {
	var lines []string
	if len(path) != 0 {
		file, err := os.Open(path)
		checkError("Cannot open blacklist\n", err)
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}
	}
	return lines
}

func createAccountList(sess *session.Session, account string) []string {
	if len(account) <= 0 {
		return getOrganizationAccounts(sess)
	}
	return []string{account}
}

func createRegionList(sess *session.Session, region string) []string {
	var regions []string
	if len(region) <= 0 {
		regions = getRegions(sess)
		return regions
	}
	return []string{region}
}

func createSession(profile string) *session.Session {
	var sess *session.Session
	if len(profile) <= 0 {
		sess = session.Must(session.NewSession(&aws.Config{
			Region: aws.String(constRegion),
		}))
		return sess
	}
	sess = session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String(constRegion)},
		Profile: profile,
	}))
	return sess
}

func accountBlacklisted(account string, blacklist []string) bool {
	var result bool
	for _, element := range blacklist {
		if element == account {
			fmt.Printf("Account %v: Blacklisted!\n", account)
			result = true
			break
		}
	}
	return result
}

func getTrails(sess *session.Session) [][]string {
	cloudtrailSvc := cloudtrail.New(sess)
	cloudTrails, err := cloudtrailSvc.DescribeTrails(&cloudtrail.DescribeTrailsInput{})
	checkError("Cannot get trails", err)
	var trails [][]string

	if len(cloudTrails.TrailList) != 0 {
		for _, trail := range cloudTrails.TrailList {
			trails = append(trails, []string{
				*trail.Name,
				*trail.S3BucketName,
				strconv.FormatBool(*trail.IsMultiRegionTrail),
				strconv.FormatBool(*trail.IsOrganizationTrail),
				strconv.FormatBool(*trail.LogFileValidationEnabled),
			})
		}
	} else {
		trails = append(trails, []string{"-", "-", "-", "-", "-"})
	}
	return trails
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func getRegions(sess *session.Session) []string {
	ec2Svc := ec2.New(sess)
	regions, err := ec2Svc.DescribeRegions(&ec2.DescribeRegionsInput{})
	checkError("Cannot get regions\n", err)
	var result []string
	for _, region := range regions.Regions {
		result = append(result, *region.RegionName)
	}
	return result
}

func getCurrentAccountID(sess *session.Session) string {
	stsSvc := sts.New(sess)
	idendtity, err := stsSvc.GetCallerIdentity(&sts.GetCallerIdentityInput{})
	checkError("Cannot get main account details\n", err)
	return *idendtity.Account
}

func getOrganizationAccounts(sess *session.Session) []string {
	orgSvc := organizations.New(sess)
	orgAccounts, err := orgSvc.ListAccounts(&organizations.ListAccountsInput{})
	checkError("Cannot get organization accounts\n", err)
	var accounts []string
	for _, account := range orgAccounts.Accounts {
		accounts = append(accounts, *account.Id)
	}

	nextToken := orgAccounts.NextToken
	for nextToken != nil {
		orgAccounts, err := orgSvc.ListAccounts(&organizations.ListAccountsInput{
			NextToken: nextToken,
		})
		checkError("Cannot fetch additional organization accounts\n", err)

		for _, account := range orgAccounts.Accounts {
			accounts = append(accounts, *account.Id)
		}
		nextToken = orgAccounts.NextToken
	}
	return accounts
}
