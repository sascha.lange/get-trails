.PHONY: build clean

build:
	go get github.com/aws/aws-sdk-go
	go get github.com/tsak/concurrent-csv-writer
	go get github.com/schollz/progressbar
	go get github.com/mitchellh/colorstring
	env GOOS=linux go build -ldflags="-s -w" -o  bin/get-trails-linux-amd64 main.go
	upx bin/get-trails-linux-amd64
	env GOOS=darwin go build -ldflags="-s -w" -o  bin/get-trails-darwin-amd64 main.go
	upx bin/get-trails-darwin-amd64
	env GOOS=windows go build -ldflags="-s -w" -o  bin/get-trails-windows-amd64.exe main.go
	upx bin/get-trails-windows-amd64.exe

clean:
	rm -rf ./bin
