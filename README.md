# Description
Tool to collect all AWS cloud trail informations from all sub accounts of an AWS organization and save the results as result.csv in your current working directory. It uses the default authentication patterns as all AWS SDKs. It will use the environment variable AWS_PROFILE, a aws credentials file or an iam role permissions.

***This tools needs to be run with an user from your master account. This user has to have permission to access all sub accounts with the aws assume role feature.***

# Limitation
- ***This tool will only query sub accounts and ignores your master account, since a assume role into the master account will fail.***
- ***It only reads available regions from the master account. If the sub account has additional regions enabled, they will not appear in the results.***

# Prerequisite
- make
- upx
- golang

# Build local
```shell
$ make
```

# Usage
## Print help
```shell
$ ./get-trails -h
```

## Get all trails from all sub accounts
```shell
$ ./get-trails
```

## Limit to one sub account for all regions
```shell
$ ./get-trails -account <sub account id>
```

## Limit to one region for all sub accounts
```shell
$ ./get-trails -region <aws region name>
```

## Limit to one region and one sub account
```shell
$ ./get-trails -account <sub account id> -region <aws region name>
```

!!!

